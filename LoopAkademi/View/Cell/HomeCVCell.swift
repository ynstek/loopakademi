//
//  HomeCVCell.swift
//  LoopAkademi
//
//  Created by Yunus Tek on 15.05.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit

class HomeCVCell: UICollectionViewCell {
    @IBOutlet var createDate: UILabel!
    
    @IBOutlet var image: UIImageView!
    @IBOutlet var storyLikeCount: UILabel!
    @IBOutlet var btnLike: UIButton!
    
    @IBOutlet var comment1View: UIView!
    @IBOutlet var commentBody1: UILabel!
    @IBOutlet var commentTitle1: UILabel!
    
    @IBOutlet var comment2View: UIView!
    @IBOutlet var commentBody2: UILabel!
    @IBOutlet var commentTitle2: UILabel!
    
    @IBOutlet var btnComment: UIButton!

}
