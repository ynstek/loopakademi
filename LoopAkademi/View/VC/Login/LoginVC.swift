//
//  LoginVC.swift
//  LoopAkademi
//
//  Created by Yunus Tek on 15.04.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class LoginVC: UIViewController {

    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var username: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        IQKeyboardManager.sharedManager().keyboardDistanceFromTextField = 70
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func login() {
        if Reachability.isConnectedToNetwork() {
            self.activityOpen()
            JsonLogin.Todo.connect(phone: username.text!, password: password.text!, completionHandler: { (result, error) in
                self.activityClose()
                if result!.Result {
                    DispatchQueue.main.async() {
                        self.performSegue(withIdentifier: "openHome", sender: nil)
//                        AlertFunctions.messageType.showOKAlert(result!.Message!, bodyMessage: result!.Name!.uppercased() + " " + result!.Surname!.uppercased())
                    }
                } else {
                    DispatchQueue.main.async() {
                        AlertFunctions.messageType.showOKAlert("Hatalı Giriş", bodyMessage: result!.Message!)
                    }
                }
            })
        }
    }
    
    @IBAction func btnLogin(_ sender: Any) {
        if isValid() {
            login()
        }
    }
    
    func isValid() -> Bool {
        self.username.text = self.username.text!.trimmingCharacters(in: CharacterSet.whitespaces)
        self.password.text = self.password.text!.trimmingCharacters(in: CharacterSet.whitespaces)

        if !username.text!.isEmpty && !password.text!.isEmpty {
            return true
        } else {
            AlertFunctions.messageType.showInfoAlert("", bodyMessage: "Lütfen bilgilerinizi kontrol ediniz!")
            return false
        }
    }
    
}
