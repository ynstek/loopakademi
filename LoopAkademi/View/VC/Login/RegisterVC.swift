//
//  RegisterVC.swift
//  LoopAkademi
//
//  Created by Yunus Tek on 18.04.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit

class RegisterVC: UIViewController {

    @IBOutlet weak var ad: UITextField!
    @IBOutlet weak var soyad: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var telefon: UITextField!
    @IBOutlet weak var sifre: UITextField!
    @IBOutlet weak var sifreTekrar: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnUyeligimiTamamla(_ sender: Any) {
        if isValid() {
            register()
        }
    }
    
    func isValid() -> Bool {
        if !ad.text!.isEmpty && !soyad.text!.isEmpty && !email.text!.isEmpty && !telefon.text!.isEmpty && !sifre.text!.isEmpty && !sifreTekrar.text!.isEmpty {
            
            if sifre.text! != sifreTekrar.text! {
                AlertFunctions.messageType.showOKAlert("UYARI!", bodyMessage: "Şifreniz uyuşmuyor. Lütfen tekrar deneyiniz.")
            } else if GlobalFunctions.isValidEmail(email.text!) && GlobalFunctions.isValidPhone(telefon.text!) {
                return true
            }
            
        } else {
            AlertFunctions.messageType.showOKAlert("UYARI!", bodyMessage: "Lütfen boş alanları doldurunuz.")
        }
        
        return false
    }
    
    func register() {
        if Reachability.isConnectedToNetwork() {
            self.activityOpen()
            JsonRegister.Todo.connect(Name: ad.text!, Surname: soyad.text!, Email: email.text!, Password: sifre.text!, Phone: telefon.text!, completionHandler: { (result, error) in
                self.activityClose()
                if result!.Result {
                    DispatchQueue.main.async() {
                        AlertFunctions.messageType.showOKAlert(result!.Message!, bodyMessage: result!.Name! + " " + result!.Surname!)
                    }
                } else {
                    DispatchQueue.main.async() {
                        AlertFunctions.messageType.showOKAlert("Hatalı Giriş", bodyMessage: result!.Message!)
                    }
                }
            })
        }
    }

    
}
