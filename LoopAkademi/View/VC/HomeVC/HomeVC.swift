//
//  HomeVC.swift
//  LoopAkademi
//
//  Created by Yunus Tek on 27.04.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit

class HomeVC: UIViewController {

    @IBOutlet var collectionView: UICollectionView!
    
    @IBOutlet var sViewHeight: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.presentTransparentNavigationBar()
        
        self.getStory()
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.leftBarButtonItem =
            GlobalFunctions.shared.newBarButton("menuIcon", action: #selector(SWRevealViewController.revealToggle(_:)), view: self.revealViewController())
        self.navigationItem.rightBarButtonItem = GlobalFunctions.shared.newBarButton("bildirim", action: #selector(self.onBildirim), view: self)
        
        GlobalFunctions.menuView(current: self)
        collectionView.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
    }
    
    func getStory() {
        if Reachability.isConnectedToNetwork() {
            self.activityOpen()
            JsonStory.Todo.connect(completionHandler: { (result, error) in
                self.activityClose()
                if result != nil {
                    DispatchQueue.main.async() {
                        GlobalVariables.shared.jsonStory = result!
                        self.collectionView.reloadData()
                    }
                }
            })
        }
    }
    
    @objc func onBildirim () {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}
