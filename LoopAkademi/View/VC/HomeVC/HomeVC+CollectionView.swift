//
//  HomeVC+CollectionView.swift
//  LoopAkademi
//
//  Created by Yunus Tek on 15.05.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import Foundation
import UIKit

extension HomeVC: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let count = CGFloat(GlobalVariables.shared.jsonStory.count)
        self.sViewHeight.constant = 93 + (count * 390)
        
        return Int(count)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! HomeCVCell
        
        let data = GlobalVariables.shared.jsonStory[indexPath.row]
        cell.image.image = nil
        DispatchQueue.main.async {
            cell.image.downloadedFrom(link: data.ImagePath)
        }

        cell.createDate.text = data.CreateDate?.ToDate().ToString()
        
        cell.btnComment.isHidden = true
        if data.CommentCount != 0 {
            cell.btnComment.isHidden = false
            cell.btnComment.setTitle("\(data.CommentCount!) Yorumun Tamamını Gör",for: .normal)
        }
        
        cell.storyLikeCount.text = "\(data.StoryLikeCount ?? 0) Kişi Beğendi"

        let likeImage = data.isLike! ? #imageLiteral(resourceName: "fav") : #imageLiteral(resourceName: "favnot")
        cell.btnLike.setImage(likeImage, for: .normal)

        // Comment
        cell.comment1View.isHidden = true
        cell.comment2View.isHidden = true
        if data.comments?.count == 1 {
            cell.comment1View.isHidden = false
            cell.commentBody1.text = data.comments![0].StudentName! + " " + data.comments![0].studentSurname!
            cell.commentTitle1.text = data.comments![0].Comment!
        }

        if data.comments?.count == 2 {
            cell.comment1View.isHidden = false
            cell.commentBody1.text = data.comments![0].StudentName! + " " + data.comments![0].studentSurname!
            cell.commentTitle1.text = data.comments![0].Comment!
            
            cell.comment2View.isHidden = false
            cell.commentBody2.text = data.comments![1].StudentName! + " " + data.comments![1].studentSurname!
            cell.commentTitle2.text = data.comments![1].Comment!
        }
        
        return cell
    }
    
    
}

extension HomeVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let data = GlobalVariables.shared.jsonStory[indexPath.row]
        var height: CGFloat = 380
        if data.comments?.count == 0 {
            height = 277
        } else if data.comments?.count == 1 {
            height = 345
        }
        
        return CGSize(width: (self.view.frame.width - 20), height: height)
    }
}

extension HomeVC: UICollectionViewDelegate {
    
}
