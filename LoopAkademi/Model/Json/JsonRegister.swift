//
//  JsonRegister.swift
//  LoopAkademi
//
//  Created by Yunus Tek on 18.04.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//


import Foundation

class JsonRegister : NSObject  {
    
    static func endpointForTodos() -> String {
        return GlobalVariables.shared.url + "/api/account/register"
    }
    
    struct Todo: Codable {
        // POST - SAVE
        static func connect(Name: String, Surname: String, Email: String, Password: String, Phone: String, completionHandler: @escaping (Login?, Error?) -> Void) {
            let endpoint = endpointForTodos()
            
            let params = [
                "Name" : Name
                ,"Surname" : Surname
                ,"Email" : Email
                ,"Password" : Password
                ,"Phone" : Phone
                ] as [String : Any]
            
            guard let todosURL = URL(string: endpoint) else {
                let error = Json.BackendError.urlError(reason: "Could not create URL")
                AlertFunctions.messageType.showOKAlert("HATA!", bodyMessage: error.localizedDescription)
                completionHandler(nil, error)
                return
            }
            
            var todosUrlRequest = URLRequest(url: todosURL)
            todosUrlRequest.httpMethod = "POST"
            
            todosUrlRequest.setValue("Application/json", forHTTPHeaderField: "Content-Type")
            
            guard let httpBody = try? JSONSerialization.data(withJSONObject: params, options: []) else {
                let error = Json.BackendError.urlError(reason: "Error httpBody")
                AlertFunctions.messageType.showOKAlert("HATA!", bodyMessage: error.localizedDescription)
                completionHandler(nil, error)
                return
            }
            
            todosUrlRequest.httpBody = httpBody
            
            let session = URLSession.shared
            session.dataTask(with: todosUrlRequest) { (data, response, error) in
                if let data = data {
                    do {
                        guard (try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String : Any]) != nil else { return }
                        
                        let todos = try JSONDecoder().decode(Login.self, from: data)
                        
                        if todos.Result {
                            UserDefaults.standard.set(todos.StudentId, forKey: "userId")
                            UserDefaults.standard.synchronize()
                            
                            GlobalVariables.shared.activeUser = todos
                        }
                        
                        completionHandler(todos , nil)
                        
                    } catch let error {
                        AlertFunctions.messageType.showOKAlert("HATA!", bodyMessage: error.localizedDescription)
                        completionHandler(nil, error)
                        return
                    }
                }
                }.resume()
        }
    }
}

