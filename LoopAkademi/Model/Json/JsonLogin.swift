//
//  JsonLogin.swift
//  LoopAkademi
//
//  Created by Yunus Tek on 15.04.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import Foundation

class JsonLogin : NSObject  {
    
    static func endpointForTodos(_ phone: String, _ password: String) -> String {
        return GlobalVariables.shared.url + "/api/account/login?Phone=\(phone)&Password=\(password)"
    }
    
    struct Todo: Codable {
        static func connect(phone: String, password: String, completionHandler: @escaping (Login?, Error?) -> Void) {
            let endpoint = endpointForTodos(phone, password)
            guard let url = URL(string: endpoint) else {
                print("Error: cannot create URL")
                AlertFunctions.messageType.showOKAlert("Error", bodyMessage: "Could not construct URL")
                let error = Json.BackendError.urlError(reason: "Could not construct URL")
                completionHandler(nil, error)
                return
            }
            let urlRequest = URLRequest(url: url)
            
            let session = URLSession.shared
            
            let task = session.dataTask(with: urlRequest) {(data, response, error) in
                guard data != nil else {
                    print("Error: did not receive data", error!)
                    AlertFunctions.messageType.showOKAlert("Error", bodyMessage: "Did not receive data\n" + error!.localizedDescription)
                    completionHandler(nil, error)
                    return
                }
                
                guard error == nil else {
                    AlertFunctions.messageType.showOKAlert("Error", bodyMessage: error!.localizedDescription)
                    completionHandler(nil, error)
                    return
                }
                
                do {
                    guard (try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String : Any]) != nil else { return }
                    
                    let todos = try JSONDecoder().decode(Login.self, from: data!)
                    
                    if todos.Result {
                        UserDefaults.standard.set(todos.StudentId, forKey: "userId")
                        UserDefaults.standard.synchronize()
                        
                        GlobalVariables.shared.activeUser = todos
                    }
                    
                    completionHandler(todos , nil)
                    return
                    
                } catch {
                    print("Error trying to convert data to JSON")
                    AlertFunctions.messageType.showOKAlert("Error", bodyMessage: error.localizedDescription)
                    print(error)
                    completionHandler(nil, error)
                }
            }
            task.resume()
        }
        
    }
}

