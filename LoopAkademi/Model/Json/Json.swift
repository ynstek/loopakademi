//
//  Json.swift
//  LoopAkademi
//
//  Created by Yunus Tek on 15.04.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import Foundation
import UIKit

private let _sharedjson = Json()
class Json : NSObject  {
    
    // MARK: - SHARED INSTANCE
    class var shared : Json {
        return _sharedjson
    }
    
    enum BackendError: Error {
        case urlError(reason: String)
        case objectSerialization(reason: String)
    }
}
