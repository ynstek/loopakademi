//
//  Login.swift
//  LoopAkademi
//
//  Created by Yunus Tek on 15.05.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import Foundation

struct Login : Codable {
    let Result : Bool
    let Message : String?
    
    let StudentId : Int?
    let Name : String?
    let Surname : String?
    let Email : String?
    let City : String?
    let Town : String?
    let Phone : String?
    let BirthDate : String?
    let ProfilePhoto : String?
    let Description : String?
}
