//
//  Story.swift
//  LoopAkademi
//
//  Created by Yunus Tek on 15.05.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import Foundation

struct Story : Codable {
    let StoryId : Int?
    let ImagePath : String?
    let isLink : Bool?
    let Link : String?
    let CreateDate : String?
    let comments : [Comment]?
    let StoryLikeCount : Int?
    let CommentCount : Int?
    let isLike : Bool?
}
