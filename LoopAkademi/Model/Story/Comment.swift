//
//  Comment.swift
//  LoopAkademi
//
//  Created by Yunus Tek on 15.05.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import Foundation

struct Comment : Codable {
    
    let StoryCommentId : Int?
    let Comment : String?
    let CreateDate : String?
    let StoryID : Int?
    let StudentID : Int?
    let StudentName : String?
    let studentSurname : String?
    let studentPhoto : String?
}
