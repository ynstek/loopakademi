//
//  AppDelegate.swift
//  LoopAkademi
//
//  Created by Yunus Tek on 15.04.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        keyboardSettings()
        
        return true
    }

    func keyboardSettings() {
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().enableDebugging = true
        
        // Done butonu
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
        IQKeyboardManager.sharedManager().toolbarDoneBarButtonItemText = "Bitti"
        //        IQKeyboardManager.sharedManager().toolbarDoneBarButtonItemImage= UIImage(named: "")
        //        IQKeyboardManager.sharedManager().toolbarTintColor = .red // UIColor.red // Done butonun rengi
        
        // Klavyenin texte uzakligi
        IQKeyboardManager.sharedManager().keyboardDistanceFromTextField = 10
        
        // Place Holderlari goster
        IQKeyboardManager.sharedManager().shouldShowToolbarPlaceholder = true
        //        IQKeyboardManager.sharedManager().placeholderFont = UIFont(name: "", size: 8)
        
        // Bos yere tiklayinca klavyeyi kapat
        IQKeyboardManager.sharedManager().shouldResignOnTouchOutside = true
        
        //        IQKeyboardManager.sharedManager().shouldPlayInputClicks = false // ?
        
        // txtyi klavyeye gore daralt
        IQKeyboardManager.sharedManager().canAdjustAdditionalSafeAreaInsets = true
        
        // Klavye rengi
        //        IQKeyboardManager.sharedManager().overrideKeyboardAppearance = true
        //        IQKeyboardManager.sharedManager().keyboardAppearance = .dark
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

