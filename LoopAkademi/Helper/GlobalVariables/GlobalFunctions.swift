//
//  GlobalFunctions.swift
//  Orca POS Mobile
//
//  Created by Yunus TEK on 21.03.2017.
//  Copyright © 2017 Orca Businesss Solutions. All rights reserved.
//

import UIKit
import Foundation

private let _sharedGlobalFunctions = GlobalFunctions()
class GlobalFunctions : NSObject, URLSessionDelegate {
    
    // MARK: - SHARED INSTANCE
    class var shared : GlobalFunctions {
        return _sharedGlobalFunctions
    }
    
    // MASK: Animations
    let duration: TimeInterval = 0.5
    let delay: TimeInterval = 0.0
    let usingSpringWithDamping: CGFloat = 0.3
    let initialSpringVelocity: CGFloat = 8
    
    func animationShake<T: UIView>(_ obj: T, _ times: CGFloat = 12) {
        // sgm = 12
        // button = 12
        // image = 12
        // label = 6
        // view = 10
        DispatchQueue.main.async() {
            let bounds = obj.bounds
            UIView.animate(withDuration: self.duration, delay: self.delay, usingSpringWithDamping: self.usingSpringWithDamping, initialSpringVelocity: self.initialSpringVelocity, options: UIViewAnimationOptions(), animations: {
                
                obj.bounds = CGRect(x: bounds.origin.x, y: bounds.origin.y, width: bounds.size.width+(bounds.size.width/times), height: bounds.size.height+(bounds.size.width/times))
                
            }, completion: nil)
            
            obj.bounds = CGRect(x: bounds.origin.x, y: bounds.origin.y, width: bounds.size.width, height: bounds.size.height)
        }
    }
    
    class func menuView(current: UIViewController, right: Bool = false) {
        current.revealViewController().rearViewRevealWidth = 260 // Menu
        
        if right == false {
            current.revealViewController().rightViewRevealWidth = 0
        } else {
            current.revealViewController().rightViewRevealWidth = 150 // Filter
        }
        current.revealViewController().rightViewRevealOverdraw = 0 // default: 60
        
    current.view.addGestureRecognizer(current.revealViewController().panGestureRecognizer())
        
    }
    class func isValidEmail(_ email:String) -> Bool {
        let emailRegEx =
            "(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
                + "~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
                + "x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
                + "z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
                + "]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
                + "9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
                + "-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
        
//        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let emailTest = NSPredicate(format:"SELF MATCHES[c] %@", emailRegEx)

        let result = emailTest.evaluate(with: email)
        
        if result == false {
            AlertFunctions.messageType.showOKAlert("UYARI", bodyMessage: "Geçerli bir E-Posta adresi girmelisin")
        }
        
        return result
    }
    
    class func isValidPhone(_ value: String, _ error: Bool = true) -> Bool {
        let PHONE_REGEX = "^((\\+)|(00))[0-9]{6,14}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        var result =  phoneTest.evaluate(with: "+9" + value)
        
        if value.count != 11 {
            result = false
        }
        
        if Int(value) == 0 {
            result = false
        }
        
        if result == false && error {
            AlertFunctions.messageType.showOKAlert("UYARI", bodyMessage: "Geçerli bir cep telefonu numarası girmelisin")
        }        
        
        return result
    }
    
    // MASK: Navigation Controller New Bar Button
    func newBarButton(_ imgName: String, action: Selector, view: UIViewController) -> UIBarButtonItem{
        let button: UIButton = UIButton(type: .custom)
        button.setImage(UIImage(named: imgName), for: UIControlState())
//        button.frame = CGRect(x: 20,y: 20,width: 23,height: 32)
        //        button.bounds = CGRect(x: 0,y: 70,width: 30,height: 80)
        button.bounds = CGRect(x: 20,y: 20,width: 23,height: 32)
        
        button.addTarget(view, action: action, for: .touchUpInside)
        let barButton = UIBarButtonItem(customView: button)
        
        return barButton
    }
    
    class func getTopController() -> UIViewController {
        var topController = UIApplication.shared.keyWindow!.rootViewController! as UIViewController
        while ((topController.presentedViewController) != nil) {
            topController = topController.presentedViewController!;
        }
        return topController
    }
    
}



