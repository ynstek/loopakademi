//
//  GlobalVariables.swift
//  LoopAkademi
//
//  Created by Yunus Tek on 15.04.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import Foundation
import UIKit

private let _sharedGlobalVariables = GlobalVariables()
class GlobalVariables : NSObject  {
    
    // MARK: - SHARED INSTANCE
    class var shared : GlobalVariables {
        return _sharedGlobalVariables
    }
    
    var activeUser: Login? = nil
    
    lazy var jsonStory : [Story] = {
        return [Story]()
    }()
//
//    lazy var selectedSeller : JsonSeller.Response? = nil
    
    let url = "http://loopakademi.loopbs.com"
}


